Feature: Invoke bonus offer full details endpoint and validate response

  Scenario: Invoke bonus offer full details endpoint and validate bonusoffer
    Given I Invoke a bonus offer full details endpoint with id "1351428"
    When I get a response
    Then I get outcomes
    And I validate "first" outcome
      | outcomeType | eventType                        | product | payoutType | payoutReason                             |
      | CASH        | ["BET_SETTLEMENT", "BET_PLACED"] | casino  | NO_PAYOUT  | No Payout for Game Reward Risk Free Bet" |

    And I validate below links
    |self|
    |bonusoffer|
    |bonusoffers|

Scenario: Create bonus offer
  Given create a bonus offer
  When I see the status code as 201
  Then i verified bonus offer id created

Scenario: Edit bonus offer
  Given Edit a bonus offer
  When I see the status code as 200



#  I use the rest assured libraries which has the built in methods. I will declare a variable for the url
#  or API and call that ref variable into a GET method and execute the urls.

  #The response will hold by Rest assured class called Response. Create a reference to that class and
  # then we can call that reference where ever required

  Scenario: Create campaign
    Given Create campaign
    When I get response of campaign creation
    Then I verified campaign id created