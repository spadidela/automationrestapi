Feature: Invoke customer monitor details endpoint and validate response\

  Scenario: Get monitor details endpoint and validate customer
    Given I Invoke a customer monitor details endpoint with customerid "101292489"
    When I validate the status code
    Then I validate monitors

#      | outcomeType | eventType                        | product | payoutType | payoutReason                             |
#      | CASH        | ["BET_SETTLEMENT", "BET_PLACED"] | casino  | NO_PAYOUT  | No Payout for Game Reward Risk Free Bet" |
#
#    And I validate below links
#    |self|x
#    |bonusoffer|
#    |bonusoffers|



#  I use the rest assured libraries which has the built in methods. I will declare a variable for the url
#  or API and call that ref variable into a GET method and execute the urls.

  #The response will hold by Rest assured class called Response. Create a reference to that class and
  # then we can call that reference where ever required