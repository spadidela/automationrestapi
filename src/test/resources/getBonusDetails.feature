# new feature
# Tags: optional

Feature: Get bonus details endpoint and validate response

  Scenario: Verify bonus details
    Given I Invoke a bonus details endpoint with bonus id "10763037"
    When I validate a response
    Then I validate links
    |self|
    |outcome-progress|
    |reward-progress|
    |bonus-offer|

