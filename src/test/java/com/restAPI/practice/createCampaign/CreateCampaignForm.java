package com.restAPI.practice.createCampaign;

import lombok.Data;

@Data
public class CreateCampaignForm {
    private String title;
    private String description;
    private String campaignType;
    private String campaignSubtype;
    private String STANDARD;
    private String validFrom;
    private String validTo;
    private String costAllocation;
    private boolean isRecurrent;
    private boolean expiryReminder;
    private boolean autoOptin;

}
