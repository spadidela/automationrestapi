package com.restAPI.practice.model;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Outcome {
        private String outcomeRef;

        private String type;

        private List<String> eventTypes;

        private String logicalConditions;

        private String value;

        private String unit;

        private String maxValue;

        private String maxUnit;

        private List<String> childrenOutcomeRefs;

        private List<ProgressionConditions> progressionConditions;

        private boolean singleAction;

        private int validity;

        private int totalNoOfGameRounds;

        private int totalAmountToBet;

        private int position;

        private PayoutInfo payoutInfo;
    }
