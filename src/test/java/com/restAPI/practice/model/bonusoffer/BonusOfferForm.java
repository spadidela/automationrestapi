package com.restAPI.practice.model.bonusoffer;

import lombok.Data;

@Data
public class BonusOfferForm {
    private String name;
    private String description;
    private String type;
    private int validity;
    private String currency;
    private boolean global;
    private boolean crossProduct;
    private String bonusOfferTag;
}
