package com.restAPI.practice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class BonusOfferFullDetailes {
    private String bonusOfferRef;
    private String name;
    private String type;
    private String validity;
    private String currency;
    private String global;
    private String status;
    private List<Outcome> outcomes;
    private  List<Link> links;


}
