package com.restAPI.practice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Products {
    private String name;

    private String productDescription;

    private int noOfGameRound;

    private List<String> includedGames;

    private List<String> overriddenGames;

    private List<String> excludedGames;

    private int contributionAmount;

}
