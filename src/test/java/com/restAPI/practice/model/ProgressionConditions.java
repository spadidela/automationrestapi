package com.restAPI.practice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProgressionConditions {
    private String type;

    private List<Products> products;

    private List<String> tags;



}
