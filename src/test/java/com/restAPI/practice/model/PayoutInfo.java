package com.restAPI.practice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class PayoutInfo {
    private String payoutType;


    private String payoutReason;




}
