package com.restAPI.practice.model.monitor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class MonitorDetails {
    private long monitorId;
    private String status;
    private String configurationId;
    private MonitorLinks _links;

}
