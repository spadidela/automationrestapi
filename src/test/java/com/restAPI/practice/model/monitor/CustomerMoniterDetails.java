package com.restAPI.practice.model.monitor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerMoniterDetails {
    private List<MonitorDetails> monitors;
    private SubLink _links;

}

