package com.restAPI.practice.model.monitor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.restAPI.practice.model.Link;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class MonitorLinks {

    @JsonProperty("get-activities")
    private List<Link> getActivities;

    private Link close;

    @JsonProperty("get-summary")
    private Link getSummary;

}
