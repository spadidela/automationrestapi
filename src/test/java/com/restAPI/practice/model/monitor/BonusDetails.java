package com.restAPI.practice.model.monitor;

import com.restAPI.practice.model.Link;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class BonusDetails {
    private long campaignId;
    private String campaignTitle;
    private  String campaignType;
    private  String campaignSubtype;
    private  String bonusOfferType;
    private  String status;
    private  Date optInDate;
    private Date expiryDate;
    private  String multiTier;
    private List<Link> links;
}
