package com.restAPI.practice.stepDefinitions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.restAPI.practice.createCampaign.CreateCampaignForm;
import com.restAPI.practice.model.BonusOfferFullDetailes;
import com.restAPI.practice.model.bonusoffer.BonusOfferForm;
import com.restAPI.practice.model.monitor.CustomerMoniterDetails;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.hasItems;

@Slf4j
public class StepDefinitions {
    protected Response response;
    private Integer oucomePosition;
    private Response response1;
    private Response getBonusDetailResponse;
    private BonusOfferFullDetailes bonusOfferFullDetailes;
    private CustomerMoniterDetails customerMoniterDetails;
    private Response bonusOfferFormResponse;
    private Response campaignResponse;
    private Response bonusOfferCreatedResponse;

    @Given("I Invoke a bonus offer full details endpoint with id {string}")
    public void i_Invoke_a_bonus_offer_full_details_endpoint(String bonusOfferId) {
        String url = "https://internal-api-qa1.unibet.com/bonusoffer/internal/bonusoffers/" + bonusOfferId + "/fulldetails";

        response = RestAssured.given().header("API_VERSION", "v1").
                when().get(url);
        System.out.println("API is invoked with header: " + url);

    }

    @When("I get a response")
    public void i_get_a_response() {
        System.out.println(response.asString());

            bonusOfferFullDetailes = response.getBody()
                .as(BonusOfferFullDetailes.class);
        System.out.println(bonusOfferFullDetailes);

    }

    @Then("I validate the response code")
    public void i_validate_the_response_code() {
//        Always you assertions to validate something

        Assert.assertEquals("Invalid Statuscode", 200, response.getStatusCode());
        System.out.println(response);
    }

    @Then("I get outcomes")
    public void i_check_for_bonusOfferId_existence() {
//        Assert.assertNotNull("bonusOfferId doesn't exist", response.getBody().jsonPath().getJsonObject("bonusOfferRef"));
        System.out.println(bonusOfferFullDetailes.getOutcomes().toString());
    }

    @Then("I validate {string} outcome")

    public void i_validate_outcome(String outcomeNumber, List<Map<String, String>> datatable) {
        if (outcomeNumber.equalsIgnoreCase("first")) {
            oucomePosition = 0;
        } else {
            if (outcomeNumber.equalsIgnoreCase("second")) {
                oucomePosition = 1;
            } else {
                oucomePosition = 2;
            }
        }

        System.out.println(bonusOfferFullDetailes);
//        String actual= bonusOfferFullDetailes.getOutcomes().get(oucomePosition).getType();
//        String expected = datatable.get(0).get("outcomeType");

        Assert.assertEquals("Invalid Type", datatable.get(0).get("outcomeType"), bonusOfferFullDetailes.getOutcomes().get(oucomePosition).getType());
        Assert.assertEquals("Invalid payoutType", datatable.get(0).get("payoutType"), bonusOfferFullDetailes.getOutcomes().get(oucomePosition).getPayoutInfo().getPayoutType());
    }

    @Then("I validate below links")
    public void i_validate_below_links(List<String> relname) {

        response.then().body("links.rel", hasItems(relname.toArray()));
        System.out.println(response.then().extract().path("links").toString());
    }

    @Given("I Invoke a customer monitor details endpoint with customerid {string}")
    public void i_Invoke_a_customer_monitor_details_endpoint_with_customerid(String customerid) {
        String url = "http://internal-api-qa1.unibet.com/transactionsummaryservice/internal/monitors?customerid=" + customerid;
        response1 = RestAssured.given().header("API_VERSION", "v1")
                .when().get(url);
    }

    @When("I validate the status code")
    public void i_validate_the_status_code() {
        Assert.assertEquals("Invalid status code", 200, response1.getStatusCode());
    }

    @Then("I validate monitors")
    public void i_validate_monitors() {

        customerMoniterDetails = response1.getBody()
                .as(CustomerMoniterDetails.class);
        System.out.println(customerMoniterDetails);

    }

    @Given("I Invoke a bonus details endpoint with bonus id {string}")
    public void i_Invoke_a_bonus_details_endpoint_with_bonus_id(String bonusid) {
        String url = "https://internal-api-qa1.unibet.com/bonusprogression/sa/bonus/{bonusId}";

//        String url = "https://internal-api-qa1.unibet.com/bonusprogression/sa?bonus={sabonus}";

            getBonusDetailResponse = RestAssured.given()
                    .header("API_VERSION", "v1")
                    .pathParam("bonusId",bonusid)
                    .when().get(url);

        System.out.println("API is invoked with header: " + url);

    }

    @When("I validate a response")
    public void i_validate_a_response() {
        getBonusDetailResponse.then().statusCode(200);

    }
    @Then("I validate links")
    public void i_validate_links(List<String> relNames) {

        getBonusDetailResponse.then().body("links.rel",hasItems(relNames.toArray()));

    }

    @Given("create a bonus offer")
    public void create_a_bonus_offer() throws JsonProcessingException {
        String url = "https://internal-api-qa1.unibet.com:443/bonusoffer/internal/bonusoffers/form";
        response = RestAssured.given()
                .header("API_VERSION", "v1")
                .when().get(url);
        System.out.println("Bonus offer form is invoked");
//       String url=  ResponseQueryUtils.GetItemValueByKeyFromLinksArrayOfItems(response,rel,createbonusoffer,href)
        String bonusOfferCreationUrl = response.then().extract().path("links[1].href");

        BonusOfferForm bonusOfferForm = getBonusOfferForm("Other", false, "GBP",
                "DEPSOTI BO", "DEPSOTI BO", "DEPOSIT", 30);

        ObjectMapper objectMapper = new ObjectMapper();

        String bonusOfferFormJson= objectMapper.writeValueAsString(bonusOfferForm);

        response= RestAssured.given().header("API_VERSION","v1")
                .header("Content-Type", "application/vnd.thin+json")
                .body(bonusOfferFormJson)
                .when().post(bonusOfferCreationUrl);

        System.out.println(bonusOfferCreatedResponse);
    }

    private BonusOfferForm getBonusOfferForm(String tag,boolean product, String currency, String decr,
                                             String name, String type, int validity) {
        BonusOfferForm bonusOfferForm = new BonusOfferForm();
        bonusOfferForm.setBonusOfferTag(tag);
        bonusOfferForm.setCrossProduct(product);
        bonusOfferForm.setCurrency(currency);
        bonusOfferForm.setDescription(decr);
        bonusOfferForm.setName(name);
        bonusOfferForm.setType(type);
        bonusOfferForm.setValidity(validity);
        return bonusOfferForm;
    }

    @When("I see the status code as {int}")
    public void i_see_the_status_code_as(int statuscode) {
        Assert.assertEquals(statuscode,response.getStatusCode());
    }


    @Then("i verified bonus offer id created")
    public void i_verified_bonus_offer_id_created(){
        Assert.assertNotNull("null",response.then().extract().path("bonusOfferRef"));
    }

    @Given("Create campaign")
    public void create_campaign() throws JsonProcessingException{
        String url = "https://internal-api-qa1.unibet.com/bonuscampaign/internal/campaigns/forms/retention";
        campaignResponse = RestAssured.given().header("API_VERSION", "v1")
                .when().get(url);
        String createCampaignUrl = campaignResponse.then().extract().path("links[1].href");

        CreateCampaignForm createCampaignForm = getCreateCampaignForm("Create campaign","description of campaign");

        ObjectMapper objectMapper = new ObjectMapper();

        String campaignFormJson = objectMapper.writeValueAsString(createCampaignForm);

        RestAssured.given().header("API_VERSION", "v1")
                .header("Content-Type", "application/json")
                .when().post(campaignFormJson);
        System.out.println("Campaign is created and response");
    }

    private CreateCampaignForm getCreateCampaignForm(String title,String description) {
        CreateCampaignForm createCampaignForm = new CreateCampaignForm();
        createCampaignForm.setTitle(title);
        createCampaignForm.setDescription(description);
        createCampaignForm.setCampaignType("RETENTION");
        createCampaignForm.setCampaignSubtype("STANDARD");
        createCampaignForm.setValidFrom("2020-07-22T00:00:00.000+02:00");
        createCampaignForm.setValidTo("2020-08-22T00:00:00.000+02:00");
        createCampaignForm.setCostAllocation("RETENTION");
        createCampaignForm.setRecurrent(false);
        createCampaignForm.setExpiryReminder(false);
        createCampaignForm.setAutoOptin(false);
        return createCampaignForm;
    }

    @When("I get response of campaign creation")
    public void i_get_response_of_campaign_creation() {
    }

    @Then("I verified campaign id created")
    public void i_verified_campaign_id_created() {

    }

    @Given("Edit a bonus offer")
    public void edit_a_bonus_offer() throws JsonProcessingException {
        String url = "https://internal-api-qa1.unibet.com:443/bonusoffer/internal/bonusoffers/{bonusOfferId}/updateform";
        response = RestAssured.given()
                .header("API_VERSION", "v1")
                .pathParam("bonusOfferId",1412429)
                .when().get(url);
        String updateBonusOfferAction = response.then().extract().path("links[2].href");
        BonusOfferForm updateBonusOfferForm = getBonusOfferForm("Other", false, "GBP",
                "DEPSOTI UPDATE", "DEPSOTI UPDATE", "DEPOSIT", 44);

        ObjectMapper objectMapper = new ObjectMapper();
     String updateBonusOfferFormJson = objectMapper.writeValueAsString(updateBonusOfferForm);
     RestAssured.given().header("API_VERSION", "v1")
             .header("Content-Type", "application/vnd.thin+json")
             .body(updateBonusOfferFormJson)
             .when().put(updateBonusOfferAction);
//             .then().statusCode(200);

    }

}
