import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty","html:target/cucumber-reports"},
        features ={"src/test/resources/getBonusDetails.feature"},
        glue={"com.restAPI.practice.stepDefinitions"},
        monochrome = true)
public class CucumberTestRunner {


}
