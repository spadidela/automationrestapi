# new feature
# Tags: optional

Feature: Create a FBM bonusOffer

  Scenario: Create a non-global bonusOffer
    Given I enter required data in bonusOffer form
      | name               | description        | type             | validity | currency | global | crossProduct | bonusOfferTag |
      | FreeBonusMoneyTest | FreeBonusMoneyTest | FREE_BONUS_MONEY | 30       | GBP      | true   | true         | Other         |

    When I enter reward amount
      | OutcomeType | SingleAction | Position | MaxUnit | MaxValue | PayoutType  | PayoutReason|
      | BONUS_MONEY | false         | 1        | GBP     | 99       | BONUS_MONEY | Bonus Money for Free Bonus Money|
