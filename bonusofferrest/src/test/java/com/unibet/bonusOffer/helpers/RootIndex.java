package com.unibet.bonusOffer.helpers;


public class RootIndex {
    public final static String BONUS_OFFER_FORM_INTERNAL = "https://internal-api-qa1.unibet.com";
    public final static String BONUS_OFFER_BASE_URL = "/bonusoffer/internal";
    public final static String BONUS_OFFER_FORM  = "/bonusoffers/form";

}
