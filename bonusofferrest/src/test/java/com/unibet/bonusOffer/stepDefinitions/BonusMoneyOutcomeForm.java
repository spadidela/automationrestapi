package com.unibet.bonusOffer.stepDefinitions;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.math.BigDecimal;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class BonusMoneyOutcomeForm {
    private String outcomeType;
    private boolean singleAction;
    @JsonIgnore
    private int validity;
    @JsonIgnore
    private int totalNoOfGameRounds;
    @JsonIgnore
    private BigDecimal totalAmountToBet;
    private int position;
    private String payoutReason;
    private String payoutType;
    private long value;
    private String unit;
    private long maxValue;
    private String maxUnit;

}
