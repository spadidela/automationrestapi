package com.unibet.bonusOffer.stepDefinitions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.unibet.bonusOffer.helpers.RootIndex;
import io.cucumber.gherkin.internal.com.eclipsesource.json.Json;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.awaitility.Awaitility;

import java.util.List;
import java.util.Map;

import static java.util.concurrent.TimeUnit.SECONDS;


@Slf4j
public class BonusOfferStepDefs {
    private Response createBonusOfferResponse;

//    private RootIndex rootIndex;

    @Given("I enter required data in bonusOffer form")
    public void i_enter_required_data_in_bonusOffer_form(List<Map<String, String>> map) throws JsonProcessingException {
        String bonusOfferForm = RootIndex.BONUS_OFFER_FORM_INTERNAL + RootIndex.BONUS_OFFER_BASE_URL + RootIndex.BONUS_OFFER_FORM;
        Response bonusOfferResponse = RestAssured
                .given().header("API_VERSION", "v1")
                .when().get(bonusOfferForm);
        String createBonusOfferUrl = bonusOfferResponse.then().extract().path("links[1].href");
        System.out.println("Create bonusoffer URL: " + createBonusOfferUrl);

        BonusOfferCreateForm createBonusOfferForm = setValuesInCreateBonusOfferForm(map);


        ObjectMapper objectMapper = new ObjectMapper();
        String bonusOfferCreateFormJson = objectMapper.writeValueAsString(createBonusOfferForm);
        log.info("create bonusOffer form in json \n" + bonusOfferCreateFormJson);

        createBonusOfferResponse = RestAssured.given()
                .header("API_VERSION", "v1")
                .header("Content-Type", "application/vnd.thin+json")
                .body(bonusOfferCreateFormJson)
                .when().post(createBonusOfferUrl.replace(":443/","/"));

        log.info("BonusOffer is created:\n" + createBonusOfferResponse.toString());

    }

    private BonusOfferCreateForm setValuesInCreateBonusOfferForm(List<Map<String, String>> map) {
        BonusOfferCreateForm bonusOfferCreateForm = new BonusOfferCreateForm();
//        List<Map<String,String>> map =  dataTable.asMaps(String.class,String.class);
        bonusOfferCreateForm.setName(map.get(0).get("name"));
        bonusOfferCreateForm.setDescription(map.get(0).get("description"));
        bonusOfferCreateForm.setType(map.get(0).get("type"));
        bonusOfferCreateForm.setValidity(Integer.parseInt(map.get(0).get("validity")));
        bonusOfferCreateForm.setCurrency(map.get(0).get("currency"));
        bonusOfferCreateForm.setGlobal(Boolean.parseBoolean(map.get(0).get("global")));
        bonusOfferCreateForm.setCrossProduct(Boolean.parseBoolean(map.get(0).get("crossProduct")));
        bonusOfferCreateForm.setBonusOfferTag(map.get(0).get("bonusOfferTag"));
        return bonusOfferCreateForm;
    }

    @When("I enter reward amount")
    public void i_enter_reward_amount(List<Map<String, String>> map) throws JsonProcessingException {

        String bonusOfferOutcomesUrl = createBonusOfferResponse.then().extract().path("links[5].href");
        Response outcomesResponse = RestAssured.given().header("API_VERSION", "v1")
                .header("Content-Type", "application/vnd.thin+json")
                .get(bonusOfferOutcomesUrl);
        String bonusMoneyOutcomeFormUrl = outcomesResponse.then().extract().path("links[1].href");
        Response bonusMoneyOutcomesResponse = RestAssured.given().header("API_VERSION", "v1")
                .header("Content-Type", "application/vnd.thin+json")
                .get(bonusMoneyOutcomeFormUrl);

        String createBonusMoneyOutcomeUrl = bonusMoneyOutcomesResponse.then().extract().path("links[1].href");

        BonusMoneyOutcomeForm bonusMoneyOutcomeForm = setValuesOfBonusMoneyOutcomeForm(map);

        ObjectMapper objectMapper = new ObjectMapper();
        String bonusMoneyOutcomeFormJson = objectMapper.writeValueAsString(bonusMoneyOutcomeForm);
        Response createBonusMoneyOutcomeResponse = RestAssured.given()
                .header("API_VERSION", "v1")
                .header("Content-Type", "application/vnd.thin+json")
                .body(bonusMoneyOutcomeFormJson)
                .when().post(createBonusMoneyOutcomeUrl);

        System.out.println(String.format("Bonus money outcome is created \n" + createBonusMoneyOutcomeResponse.getBody().jsonPath().get()));
    }

//    private Boolean getResponse(String createBonusMoneyOutcomeUrl, String bonusMoneyOutcomeFormJson) {
//        Response createBonusMoneyOutcomeResponse = RestAssured.given().header("API_VERSION", "V1")
//                .header("Content-Type", "application/vnd.thin+json")
//                .body(bonusMoneyOutcomeFormJson)
//                .when().post(createBonusMoneyOutcomeUrl);
//
//        if (createBonusMoneyOutcomeResponse.statusCode() == 200){
//            System.out.println("Bonus money outcome is created" + createBonusMoneyOutcomeResponse);
//            return true;}
//        else
//        return false;
//    }

    private BonusMoneyOutcomeForm setValuesOfBonusMoneyOutcomeForm(List<Map<String, String>> map) {
        BonusMoneyOutcomeForm bonusMoneyOutcomeForm = new BonusMoneyOutcomeForm();
        bonusMoneyOutcomeForm.setOutcomeType(map.get(0).get("OutcomeType"));
        bonusMoneyOutcomeForm.setSingleAction(Boolean.parseBoolean(map.get(0).get("SingleAction")));
        bonusMoneyOutcomeForm.setPosition(Integer.parseInt(map.get(0).get("Position")));
        bonusMoneyOutcomeForm.setUnit(map.get(0).get("MaxUnit"));
        bonusMoneyOutcomeForm.setValue(Long.parseLong(map.get(0).get("MaxValue")));
        bonusMoneyOutcomeForm.setMaxUnit(map.get(0).get("MaxUnit"));
        bonusMoneyOutcomeForm.setMaxValue(Long.parseLong(map.get(0).get("MaxValue")));
        bonusMoneyOutcomeForm.setPayoutType(map.get(0).get("PayoutType"));
        bonusMoneyOutcomeForm.setPayoutReason(map.get(0).get("PayoutReason"));
        return bonusMoneyOutcomeForm;
    }

}

