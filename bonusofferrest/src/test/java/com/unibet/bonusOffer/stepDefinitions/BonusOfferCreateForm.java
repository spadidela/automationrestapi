package com.unibet.bonusOffer.stepDefinitions;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class BonusOfferCreateForm {
    private String name;
    private String description;
    private String type;
    private int validity;
    private String currency;
    private boolean global;
    private boolean crossProduct;
    private String bonusOfferTag;

}
